api = 2
core = 7.x

; Contributed modules used by Drupress.

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[advanced_help][version] = "1.4"
projects[advanced_help][subdir] = "contrib"

projects[backup_migrate][version] = "3.5"
projects[backup_migrate][subdir] = "contrib"

projects[better_formats][version] = "1.0-beta2"
projects[better_formats][subdir] = "contrib"

projects[colorbox][version] = "2.13"
projects[colorbox][subdir] = "contrib"

projects[ctools][version] = "1.14"
projects[ctools][subdir] = "contrib"

projects[diff][version] = "3.3"
projects[diff][subdir] = "contrib"

projects[drupress_support][version] = "1.x-dev"
projects[drupress_support][subdir] = "contrib"

projects[entity][version] = "1.9"
projects[entity][subdir] = "contrib"

projects[features][version] = "2.10"
projects[features][subdir] = "contrib"

projects[features_extra][version] = "1.0"
projects[features_extra][subdir] = "contrib"

projects[field_group][version] = "1.6"
projects[field_group][subdir] = "contrib"

projects[fontyourface][version] = "2.8"
projects[fontyourface][subdir] = "contrib"

projects[gravatar][version] = "1.1"
projects[gravatar][subdir] = "contrib"

projects[insert][version] = "1.4"
projects[insert][subdir] = "contrib"

projects[jquery_update][version] = "2.7"
projects[jquery_update][subdir] = "contrib"

projects[libraries][version] = "2.3"
projects[libraries][subdir] = "contrib"

projects[login_destination][version] = "1.4"
projects[login_destination][subdir] = "contrib"

projects[mollom][version] = "2.15"
projects[mollom][subdir] = "contrib"

projects[nodeformsettings][version] = "2.x-dev"
projects[nodeformsettings][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[publish_button][version] = "1.1"
projects[publish_button][subdir] = "contrib"

projects[radioactivity][version] = "2.12"
projects[radioactivity][subdir] = "contrib"

projects[realname][version] = "1.3"
projects[realname][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.7"
projects[token][subdir] = "contrib"

projects[views][version] = "3.20"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.4"
projects[views_bulk_operations][subdir] = "contrib"

projects[wysiwyg][version] = "2.5"
projects[wysiwyg][subdir] = "contrib"

; Themes.

projects[bootstrap][version] = "3.20"
projects[bootstrap][subdir] = "contrib"

projects[shiny][version] = "1.7"
projects[shiny][subdir] = "contrib"

; Libraries.

libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.x.zip"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][destination] = "libraries"

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"
